<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;

class JobMajor extends Model
{
    protected $table = 'job_majors';
    protected $guarded = []; 
}
