<?php

namespace App\Repositories\Profile\Contract;

use App\Core\Repositories\Contract\BaseRepositoryInterface;
use Illuminate\Http\UploadedFile;
use App\Models\Users\UserMazii;

interface ProfileRepositoryInterface extends BaseRepositoryInterface {
    public function getPrivateProfile($userId);
    
    public function showProfileByUserId($userId);
}