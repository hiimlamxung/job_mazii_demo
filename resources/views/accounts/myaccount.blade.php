@extends('layouts.master')
@section('after-script')
<script src="{{ url('myJs/accounts/myaccount.js') }}"></script>
@endsection
@section('main')
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-4">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="col-xs-12 profile_left">
                            <div class="profile_img">
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    <img class="img-responsive avatar-view"
                                        src="{{ !is_null($account->profile->image) ? $account->profile->image : url('images/default.png') }}"
                                        alt="Avatar" title="Change the avatar">
                                </div>
                            </div>
                            <h3>{{ $account->username }}</h3>
                            @if ($account->user_company)
                              <h5>Tài khoản doanh nghiệp: <span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i>&#160 Đã xác  thực</span></h5>
                            @endif
                            <ul class="list-unstyled user_data">
                                <li><i class="fa fa-envelope-o"></i> {{ $account->email }}
                                </li>
                                <li><i class="fa fa-calendar"></i>
                                    {{ $account->created_at->format('d-m-Y') . ' ( ' . $account->created_at->diffForHumans() . ' )' }}
                                </li>
                            </ul>
                            @if ($account->id !== $guard->id)
                                <a class="btn btn-danger btn-user-block" data-id="{{ $account->id }}"><i
                                        class="fa fa-lock"></i> Block</a>
                            @endif
                            <br />

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-8">
              <div class="x_panel">
                <div class="x_title">
                  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#edit-profile" id="profile-tab" role="tab" data-toggle="tab" aria-expanded="true">Thông tin</a>
                    </li>
                    <li role="presentation" class=""><a href="#edit-password" role="tab" id="password-tab" data-toggle="tab" aria-expanded="false">Mật khẩu</a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
        
                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <div id="myTabContent" class="tab-content">
                      <div role="tabpanel" class="tab-pane fade active in" id="edit-profile" aria-labelledby="profile-tab">
                        <!-- start form for validation -->
                        <form data-parsley-validate="" id="frm_update_info">
                          @csrf
                          <label for="fullname">Name:</label>
                          <input type="text" class="form-control" name="username" value="{{ $account->username }}" required>
        
                          <label for="email">Email:</label>
                          <input type="email" class="form-control" disabled name="email" value="{{ $account->email }}">
                          
                          <br>
                          <button class="btn btn-success btn-update">Cập nhật</button>
                        </form>
                        <!-- end form for validations -->
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="edit-password" aria-labelledby="password-tab">
                        <div role="tabpanel" class="tab-pane fade active in" id="edit-profile" aria-labelledby="profile-tab">
                          <!-- start form for validation -->
                          <form data-parsley-validate="" id="frm_change_pass">
                            @csrf
                            <label for="password">Mật khẩu hiện tại :</label>
                            <input type="password" class="form-control" name="current_password" required="">

                            <label for="password">Mật khẩu mới :</label>
                            <input type="password" class="form-control" name="password" required="">
                            
                            <label for="password">Nhập lại mật khẩu mới:</label>
                            <input type="password" name="password_confirmation" class="form-control" required="">  
                            <br>
                            <button class="btn btn-success btn-password">Thay đổi</button>
                          </form>
                      </div>
                    </div>
                  </div>
                  
        
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
