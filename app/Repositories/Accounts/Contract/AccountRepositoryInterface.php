<?php

namespace App\Repositories\Accounts\Contract;

use App\Core\Repositories\Contract\BaseRepositoryInterface;

interface AccountRepositoryInterface extends BaseRepositoryInterface {
 
}