<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Jobs\Contract\JobRepositoryInterface;
use App\Repositories\Jobs\JobRepository;
use App\Repositories\Profile\Contract\ProfileRepositoryInterface;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\Accounts\Contract\AccountRepositoryInterface;
use App\Repositories\Accounts\AccountRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            JobRepositoryInterface::class,
            JobRepository::class
        );
        $this->app->bind(
            ProfileRepositoryInterface::class,
            ProfileRepository::class
        );
        $this->app->bind(
            AccountRepositoryInterface::class,
            AccountRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}