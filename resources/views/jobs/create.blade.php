@extends('layouts.master')
@section('after-css')
    <style>
        .box-body {
            padding: 10px;
        }

        img {
            border-radius: 10px;
        }
        span.require {
            color: red;
        }
    </style>
@endsection
@section('after-script')
    <script src="{{url('ckeditor/ckeditor.js')}}"></script>
    <script src="{{ asset('myJs/jobs/create.js') }}"></script>
@endsection

@section('main')
    <div class="row">
        <a href="{{ URL::previous() }}">
            <h5> <i class="fa fa-backward" aria-hidden="true"></i> Trở về : </h5>
        </a>
        <div class="page-title">
            <div class="title_left">
                <h3>Đăng bài tuyển dụng:</h3>
            </div>
        </div>
        <form action="" method="POST" enctype="multipart/form-data" id="frm_create_job">
            <div class="box-body row">
                @csrf
                <div class="form-group col-md-12">
                    <label>Ảnh đại diện bài viết:</label>
                    <div class="col-xs-12 text-center" style="margin-bottom: 20px;">
                        <img src="" alt="" width="250" id="current_img">
                    </div>
                    <input type="file" class="form-control" name="avatar" id="avatar" placeholder="Hình ảnh" accept="image/*">
                </div>
                <div class="form-group col-md-12">
                    <label class="title">Tiêu đề:</label><span class="require">*</span>
                    <input type="text" class="form-control auto-pinyin" name="title" placeholder="Tiêu đề...">
                </div>
                <div class="form-group col-xs-12">
                    <label class="title">Vị trí tuyển dụng:</label><span class="require">*</span>
                    <input type="text" class="form-control auto-pinyin" name="on_position"
                        placeholder="Vị trí tuyển dụng..." >
                </div>
                <div class="row col-xs-12">
                    <div class="form-group col-xs-6">
                        <label class="title">Chuyên ngành:</label><span class="require">*</span>
                        <select name="majors[]" class="tag_majors col-xs-12" multiple>
                            @foreach ($listMajors as $k => $value)
                                <option value="{{ $k }}">
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row col-xs-12">
                    <div class="form-group col-xs-12">
                        <label for="">Chọn khu vực:</label><span class="require">*</span><br>
                        <div class="row col-xs-6 pull-left">
                            <label class="checkbox-inline">
                                <input type="checkbox" class="country vn_country" value="vn" name="country[]">Việt Nam
                            </label><br>
                            <select name="province[]" class="tag_province_vn col-xs-12" multiple>
                                @foreach ($listVnProvince as $k => $value)
                                    <option value="{{ $k }}">
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row col-xs-6 pull-right">
                            <label class="checkbox-inline">
                                <input type="checkbox" class="country jp_country" value="jp" name="country[]">Nhật Bản
                            </label><br>
                            <select name="province[]" class="tag_province_jp col-xs-12" multiple>
                                @foreach ($listJpProvince as $k => $value)
                                    <option value="{{ $k }}">
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>


                    </div>
                </div>
                <div class="form-group col-xs-6">
                    <label>Hình thức làm việc:</label><span class="require">*</span>
                    <select name="type" class="form-control">
                            @foreach ($listTypes as $k => $value)
                                <option value="{{ $k }}" >{{ $value }}</option>
                            @endforeach
                    </select>
                </div>
                <div class="form-group col-xs-6">
                    <label class="title">Mức lương:</label><span class="require">*</span>
                    <input type="text" class="form-control auto-pinyin" name="salary" placeholder="Nhập mức lương...">
                </div>
                <div class="form-group col-md-12">
                    <label>Mô tả công việc:</label><span class="require">*</span>
                    <textarea class="form-control none-resize auto-pinyin ckeditor" name="description" id="description_job"  placeholder="Mô tả..."
                        rows="3"></textarea>
                </div>
                <div class="form-group col-md-12">
                    <label>Yêu cầu ứng viên:</label><span class="require">*</span>
                    <textarea class="form-control none-resize auto-pinyin" placeholder="Mô tả..." name="require"
                        rows="3"></textarea>
                </div>
                <div class="form-group col-md-12">
                    <label>Quyền lợi được hưởng:</label><span class="require">*</span>
                    <textarea class="form-control none-resize auto-pinyin" placeholder="Mô tả..." name="benifit"
                        rows="3"></textarea>
                </div>
                <div class="form-group col-md-12">
                    <label>Đường Link bài viết gốc:</label>
                    <input type="text" class="form-control auto-pinyin" name="link" placeholder="Nhập đường dẫn...">
                </div>
            </div>
            <div class="box-footer clearfix">
                <button type="submit" class="pull-right btn btn-primary" id="btn-send-news">Đăng bài
                    <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </form>
    </div>

@endsection
