<?php

namespace App\Http\Controllers\Backend\Jobs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Jobs\Contract\JobRepositoryInterface;
use App\Repositories\Profile\Contract\ProfileRepositoryInterface;
use App\Models\Jobs\JobMajor;
use JavaScript;
use App\Models\Jobs\Job;
use App\Core\Traits\ApiResponser;
use App\Models\Users\Profile;
use App\Http\Requests\Jobs\StoreJobRequest;
use App\Http\Requests\Jobs\UpdateJobRequest;

class JobController extends Controller
{
    use ApiResponser;

    protected $job;
    protected $profile;
    public function __construct(JobRepositoryInterface $job,ProfileRepositoryInterface $profile)
    {
        $this->job = $job;
        $this->profile = $profile;
    }
    public function index(Request $request){ 
        $listTypes = array_flip(config('job.type'));
        $listVnProvince = config('job.province.vn');
        $listJpProvince = config('job.province.jp');

        $listMajors = JobMajor::pluck('name','id'); 
        $dataMajors = [];
        foreach($listMajors as $key => $major){
            $dataMajors[] = [
                'id' => $key,
                'text' => $major
            ];
        }
        
        $type       = $request->get('type','all'); 
        $active     = $request->get('active','all');
        $majors     = $request->get('majors', []);
        $country    = $request->get('country', []);
        $province   = $request->get('province', []);
        $keyword    = trim($request->get('keyword',''));

        //query
        $list_job = auth()->user()->job();
        if(!empty($keyword)){
            $list_job = $list_job->where('title','like',"%$keyword%");
        }else{
            if($type !== 'all'){
                $list_job = $list_job->where('type',$type);
            }
            if($active !== 'all'){
                $list_job = $list_job->where('active',$active);
            }
            if ($majors !== []) {
                $list_job          = $list_job->where(function ($q) use ($majors) {
                    foreach ($majors as $index => $keyw) {
                        $q->orWhereLike('majors', '"' . $keyw . '"');
                    }
                });
                $appends['majors']  = $majors;
            }
            $classify = $this->job->classifyLocationParams($country, $province); 
            $list_job = $list_job->where(function ($query) use ($classify) {
                foreach ($classify as $country => $provinces) {
                    $query->orWhere(function ($q) use ($country, $provinces) {
                        $q->whereLike('country', $country);
                        $q->where(function ($p) use ($provinces) {
                            foreach ($provinces as $province) {
                                $p->orWhereLike('province', $province);
                            }
                        });
                    });
                }
            });
        }
        $list_job = $list_job->latest()->paginate(10);
        JavaScript::put([
            'url_list_job' => route('backend.jobs.index'),
            'url_active_job' => route('backend.jobs.activejob'),
            'url_del_job' => route('backend.jobs.deljob'),
            'dataMajors' => $dataMajors,
            'listVnProvince' => $listVnProvince,
            'listJpProvince' => $listJpProvince,
        ]);
        return view('jobs.index',compact(['type','active','listTypes','listMajors','listVnProvince','listJpProvince','keyword','list_job']));
    }

    public function activeJob(Request $request){
        if($request->ajax() && $request->has('id') && $request->has('active')){
            $id = $request->get('id');
            $active = $request->get('active');
            $update = $this->job->activeOrHideJob($id,$active);
            if($update){
                return $this->success('update success');
            }
        }
        return $this->error('Bad request',400);  
    }

    public function delJob(Request $request){
        if($request->ajax() && $request->has('id')){
            $id = $request->get('id');
            $del = $this->job->delJob($id);
            if($del){
                return $this->success('delete success');
            }
        }
        return $this->error('Bad request',400); 
    }

    public function listCandidate(Request $request, Job $job){
        $sort = $request->get('sort');
        $sort = (in_array($sort,['asc','desc'])) ? $sort : 'desc';
        $keyword = trim($request->get('keyword'));

        //Query
        $list_users = $job->Applies();
        if(!empty($keyword)){
            $list_users = $list_users->wherePivot('user_email','like',"%$keyword%");
        }
        $list_users = $list_users->orderBy('job_applies.created_at',$sort)->paginate(20);
        $total_userApplies = $job->Applies()->count();

        Javascript::put([
            'url_show_user' => route('backend.jobs.showprofile'),
        ]);
        return view('jobs.candidate',compact(['job','sort','keyword','list_users','total_userApplies']));
    }

    public function showProfile(Request $request){
        if($request->ajax() && $request->has('userId')){
            $userId = $request->get('userId');
            if(!Profile::where('user_id',$userId)->active()->exists()){
                return $this->error('Profile not found',404);
            }
            $profile = $this->profile->showProfileByUserId($userId);
            return $this->success($profile);
        }
        return $this->error('Bad request',400); 
    }

    public function create(){
        $listTypes = array_flip(config('job.type'));
        $listMajors = JobMajor::pluck('name','id'); 
        $listVnProvince = config('job.province.vn');
        $listJpProvince = config('job.province.jp');
        $dataMajors = [];
        foreach($listMajors as $key => $major){
            $dataMajors[] = [
                'id' => $key,
                'text' => $major
            ];
        }

        Javascript::put([
            'dataMajors' => $dataMajors,
            'listVnProvince' => $listVnProvince,
            'listJpProvince' => $listJpProvince,
            'url_store_job' => route('backend.jobs.store'),
            'url_list_job' => route('backend.jobs.index'),
        ]);
        return view('jobs.create',compact(['listMajors','listVnProvince','listJpProvince','listTypes']));
    }

    public function store(StoreJobRequest $request){
        $user = auth()->user();
        $params = $request->only([
            'avatar','title', 'description', 'require', 'benifit', 'salary', 'country', 'province', 'majors', 'type', 'on_position','link'
        ]);
        //handle province (get all provinces of country)
        $params['province'] = (!isset($params['province']) || !$params['province']) ? [] : $params['province'];

        $params['user_id']  = $user->userId;
        $params['active']   = ($user->user_company) ? 1 : 0;
        if($store = $this->job->storeJob($params)){
            return $this->success('create success');
        }
        return $this->error('create failed',500);
    }

    public function edit(Request $request,Job $job){
        $listTypes = array_flip(config('job.type'));
        $listMajors = JobMajor::pluck('name','id'); 
        $listVnProvince = config('job.province.vn');
        $listJpProvince = config('job.province.jp');
        $dataMajors = [];
        foreach($listMajors as $key => $major){
            $dataMajors[] = [
                'id' => $key,
                'text' => $major
            ];
        }

        Javascript::put([
            'dataMajors' => $dataMajors,
            'listVnProvince' => $listVnProvince,
            'listJpProvince' => $listJpProvince,
            'url_list_job' => route('backend.jobs.index'),
            'url_update_job' => route('backend.jobs.update'),
        ]); 

        return view('jobs.edit',compact(['job','listMajors','listVnProvince','listJpProvince','listTypes']));
    }

    public function update(UpdateJobRequest $request){
        $user = auth()->user();
        $id = $request->get('id');
        $params = $request->only([
            'avatar','title', 'description', 'require', 'benifit', 'salary', 'country', 'province', 'majors', 'type', 'on_position','link'
        ]);
        //handle province (get all provinces of country)
        $params['province'] = (!isset($params['province']) || !$params['province']) ? [] : $params['province'];

        if($update = $this->job->updateJob($params, $id)){
            return $this->success('Update suceess');
        }
        return $this->error('Update failed',500);
    }
}
