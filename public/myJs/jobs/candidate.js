$(function () {
    var location = window.location;
    //sort
    $('.sort_select').change(function () {
        var sort = $(this).val();
        location.href = location.origin + location.pathname + `?sort=${sort}`;
    });

    //search keyword
    $('.btn_searh_keyword').click(function () {
        var keyw = $('.input-search').val();
        location.href = location.origin + location.pathname + `?keyword=${keyw}`;
    });

    //click show user
    $('.btn_show_user').click(function () {
        var id = $(this).attr('data-id');
        $.ajax({
            type: "post",
            url: url_show_user,
            data: {
                userId: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.status == 200) {
                    let profile = response.data;
                    let arr_pro = Object.keys(profile);
                    $.each(arr_pro, function (index, value) {
                        profile[value] = (profile[value] === 'Không công khai') ? '<i class="fa fa-lock" aria-hidden="true"></i>' : profile[value];
                    })

                    $("#name").html(' ' + profile.name);
                    $("#sex").html((profile.sex == 1) ? ' Nam' : ' Nữ');
                    $("#birthday").html((profile.birthday) ? ' ' + profile.birthday : '');
                    $("#country").html((profile.country) ? ' ' + profile.country : '');
                    $("#level").html((profile.level) ? ' N' + profile.level : ' N1');
                    $("#email").html((profile.email) ? ' ' + profile.email : '');
                    $("#address").html((profile.address) ? ' ' + profile.address : '');
                    $("#phone").html((profile.phone) ? ' ' + profile.phone : '');
                    $("#introduction").html((profile.introduction) ? ' ' + profile.introduction : '');
                }
            },
            error: function () {
                $(".label_text").html(' ');
            }
        });
    })
})
