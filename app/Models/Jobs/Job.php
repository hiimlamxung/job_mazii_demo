<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Job extends Model
{
    protected $table = 'jobs';
    protected $guarded = [];
    protected $casts   = ['country' => 'array', 'province' => 'array', 'majors' => 'array'];

    /**relationships */
    public function Applies()
    {
        return $this->belongsToMany(User::class, 'job_applies', 'job_id', 'user_id')->withTimestamps()->withPivot(['user_email']);
    }

    /**End relationships */
    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%' . $value . '%');
    }
}
