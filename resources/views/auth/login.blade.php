@extends('auth.layout')
@section('main')
<form action="{{ route('backend.login') }}" method="POST">
    {{ csrf_field() }}
    <h1>Login Form</h1>
    <div>
        <input type="email" class="form-control" placeholder="Press Email..." name="email" value="{{ old('email') }}" required="" />
    </div>
    <div>
        <input type="password" class="form-control" placeholder="Press Password..." name="password"  />
    </div>
    <div>
        <input type="hidden" class="form-control" name="active" value="1" />
    </div>
    <div>
        <button class="btn btn-default" type="submit">Log in</button>
    </div>
    <span class="text-danger">
        @if ($errors->any())
            <h4>{{ $errors->first() }}</h4>
        @endif
    </span>
    <div class="clearfix"></div>

    <div class="separator">
        <p class="change_link">New to site?
        <a href="https://mazii.net/user/register" target="_blank" class="to_register"  style="color: blue"> Create Account at Mazii.net</a>
        </p>

        <div class="clearfix"></div>
        <br />

        <div>
        <h1><i class="fa fa-paw"></i> {{ config('app.team') }} Team!</h1>
        <p>©2021 All Rights Reserved.</p>
        </div>
    </div>
</form>
@endsection