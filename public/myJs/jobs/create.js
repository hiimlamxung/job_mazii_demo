$(function(){
    //image
    $("#avatar").change(function(){
        readURL(this);
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#current_img').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#current_img').attr('src', '');
        }
    }
    //Ckeditor
    CKEDITOR.replace('description_job', { //set width and height
        width: '100%',
        height: 800,
    });
    CKEDITOR.config.pasteFilter  = null; //copy paste content giữ nguyên format, css


    //load Tags
    var loadTags = function(selector,datas,place_holder){
        $(selector).select2({
            data: datas,
            tokenSeparators: [','], 
            placeholder: place_holder,
            /* the next 2 lines make sure the user can click away after typing and not lose the new tag */
            selectOnClose: false, 
            closeOnSelect: false
        });
    }
    loadTags('.tag_majors',dataMajors,'Chọn chuyên ngành');
    loadTags('.tag_province_vn',listVnProvince,'Chọn tỉnh thành');
    loadTags('.tag_province_jp',listJpProvince,'Chọn tỉnh thành');

    //Checkbox select Country
    $(".country").each(function(){
        let country = $(this).val();
        if(!$(this).is(':checked')){
            $(".tag_province_" + country).attr('disabled',true);
        }
    })
    $(".country").each(function(){
        let country = $(this).val();
        $(this).click(function(){
            if($(this).is(':checked')){
                $(".tag_province_" + country).removeAttr('disabled');
            }else{
                $(".tag_province_" + country).attr('disabled',true);
                $(".tag_province_" + country).val(null).trigger("change");
            }
        });
    })

    //Submit form create
    $("#frm_create_job").submit(function(e){
        e.preventDefault();

        //Cập nhật lại value của ckeditor.Thêm đoạn này tránh lỗi value của textarea ko đươc gửi đi với request
        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }

        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_store_job,
            type: 'POST',
            data: formData,
            processData: false,  // when use FromData,should disable 2 this option
            contentType: false, // when use FromData,should disable 2 this option
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            success: function(res){
                notify('Thêm thành công !', 'success');
                setTimeout(() => {
                    window.location.href = url_list_job;
                }, 2000);
            },
            error: function (res) {
                data = $.parseJSON(res.responseText)
                notify(data.message, 'warning');
            }
        });
    });

    //Submit form edit
    $("#frm_edit_job").submit(function(e){
        e.preventDefault();

        //Cập nhật lại value của ckeditor.Thêm đoạn này tránh lỗi value của textarea ko đươc gửi đi với request
        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }

        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_update_job,
            type: 'POST',
            data: formData,
            processData: false,  // when use FromData,should disable 2 this option
            contentType: false, // when use FromData,should disable 2 this option
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            success: function(res){
                notify('Cập nhật thành công !', 'success');
            },
            error: function (res) {
                data = $.parseJSON(res.responseText)
                notify(data.message, 'warning');
            }
        });
    });
})