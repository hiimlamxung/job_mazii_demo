<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'namespace' => 'Backend'
],function(){
    /**
     * -----------------------
     * Before login
     * -----------------------
     */
    Route::group([
        'namespace' => 'Auth',
        'middleware' => 'guest'
    ], function(){
        Route::get('/', 'LoginController@index')->name('backend.index');
        Route::post('login', 'LoginController@login')->name('backend.login');
    });

    /**
     * -----------------------
     * After login
     * -----------------------
     */
    Route::group([
        'middleware' => 'not.login'
    ],function(){
        Route::group([
            'prefix' => 'dashboard'
        ], function(){
            Route::get('/', 'HomeController@index')->name('backend.dashboard');
            Route::get('logout', 'HomeController@logout')->name('backend.logout');
        });

        Route::group([
            'prefix' => 'jobs',
            'namespace' => 'Jobs',
        ],function(){
            Route::get('/list','JobController@index')->name('backend.jobs.index');
            Route::post('/activejob','JobController@activeJob')->name('backend.jobs.activejob');
            Route::delete('/deljob','JobController@delJob')->name('backend.jobs.deljob');
            Route::get('/candidate/{job}','JobController@listCandidate')->name('backend.jobs.candidate')->where('job','[0-9]+');
            Route::post('/showprofile','JobController@showProfile')->name('backend.jobs.showprofile');
            Route::get('/create','JobController@create')->name('backend.jobs.create');
            Route::post('/store','JobController@store')->name('backend.jobs.store');
            Route::get('/edit/{job}','JobController@edit')->name('backend.jobs.edit')->where('job','[0-9]+');
            Route::post('/update','JobController@update')->name('backend.jobs.update');
        });

        Route::group([
            'prefix' => 'accounts',
            'namespace' => 'Accounts'
        ],function(){
            Route::get('/myaccount','AccountController@myaccount')->name('backend.accounts.myaccount');
            Route::post('/updateusername','AccountController@updateUsername')->name('backend.accounts.updateusername');
            Route::post('/updatepass','AccountController@updatePassWord')->name('backend.accounts.updatepassword');
        });
    });
});