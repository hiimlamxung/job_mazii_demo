<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;

class RedirectIfNotLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(!auth()->guard($guard)->check()){
            return redirect()->route('backend.index');
        } 
        $user = auth()->guard($guard)->user()->load('profile');
        View::share('guard', $user);
        return $next($request);
    }
}
