$(function(){
    $("#frm_update_info").submit(function(e){
        e.preventDefault();

        var formData = new FormData($(this)[0]);
        $.ajax({
            type: "POST",
            url: url_update_username,
            data: formData,
            processData: false,  // when use FromData,should disable 2 this option
            contentType: false, // when use FromData,should disable 2 this option
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            success: function (res) {
                if(res.status == 200){
                    notify('Cập nhật thành công !', 'success');
                }
            }
        });
    });

    $("#frm_change_pass").submit(function(e){
        e.preventDefault();

        var formData = new FormData($(this)[0]);
        $.ajax({
            type: "POST",
            url: url_update_password,
            data: formData,
            processData: false,  // when use FromData,should disable 2 this option
            contentType: false, // when use FromData,should disable 2 this option
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
            success: function (res) {
                if(res.status == 200){
                    notify('Cập nhật thành công !', 'success');
                }
            },
            error: function(response){
                let res = response.responseJSON;
                notify(res.message, 'warning');
            }
        });
    });
})