@extends('layouts.master')
@section('after-script')
    <script src="{{ url('myJs/jobs/index.js') }}"></script>
@endsection
@section('main')
    <div class="col-md-12 col-xs-12" style="position: -webkit-sticky; position: sticky; top: 0;">
        <div class="x_panel">
            <div class="x_title">
                <h2 class="col-md-3 col-xs-3">Danh sách bài viết tuyển dụng: </h2>
                <hr><br>
                <div class="row">
                    <div class="btn-group col-xs-2">
                        <b>Hình thức làm việc:</b>
                        <select name="type" class="form-control type_select">
                            <option value="all" {{ $type === 'all' ? 'selected' : '' }}>Tất cả</option>
                            @foreach ($listTypes as $k => $value)
                                <option value="{{ $k }}" {{ (string) $type == (string) $k ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="btn-group col-xs-2">
                        <b>Trạng thái:</b>
                        <select name="active" class="form-control active_select">
                            <option value="all">Tất cả</option>
                            <option value="1">Hiển thị</option>
                            <option value="0">Ẩn</option>
                        </select>
                    </div>
                    <div class="btn-group col-xs-4">
                        <b>Chuyên ngành:</b>
                        <select name="majors" class="tag_majors col-xs-12" multiple>
                            @foreach ($listMajors as $k => $value)
                                <option value="{{ $k }}">
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4">
                        <b>Từ khóa:</b>
                        <input type="text" class="form-control input-search" name="search"
                            placeholder="Nhập tiêu đề bài viết ..." value="{{ $keyword }}">
                        <button class="btn btn-sm btn-primary btn_searh_keyword">Tìm kiếm theo từ khóa</button>
                    </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="row">
                    {{-- <div class="col-xs-4">
                        <b>Chọn quốc gia:</b>
                        <div>
                            <label class="checkbox-inline">
                                <input type="checkbox" class="country vn_country" value="vn" name="country">Việt Nam
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" class="country jp_country" value="jp" name="country">Nhật Bản
                            </label>
                        </div>
                    </div> --}}
                    <div class="col-xs-12">
                        <b>Chọn quốc gia:</b>
                    </div>
                    <div class="btn-group col-xs-4">
                        <label class="checkbox-inline">
                            <input type="checkbox" class="country vn_country" value="vn" name="country">Việt Nam
                        </label><br>
                        <b>Chọn tỉnh/Thành phố của Việt Nam:</b>
                        <select name="province" class="province tag_province_vn col-xs-12" multiple>
                            @foreach ($listVnProvince as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="btn-group col-xs-4">
                        <label class="checkbox-inline">
                            <input type="checkbox" class="country jp_country" value="jp" name="country">Nhật Bản
                        </label><br>
                        <b>Chọn tỉnh/Thành phố của Nhật Bản:</b>
                        <select name="province" class="province tag_province_jp col-xs-12" multiple>
                            @foreach ($listJpProvince as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button class="btn btn-sm btn-primary btn_filter">Lọc bài viết</button>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-2 col-xs-2">Kết quả: {{ $list_job->total() }} bài viết</div><br>
            <small style="color: red;" class="col-md-5">Click vào tiêu đề để xem bài viết trên trên trang Mazii</small>
            <div class="x_content">
                <div class="table-responsive">
                    {{-- <div class="text-center">{{ $list_jobs->links() }}</div> --}}
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <th class="text-center">Hình ảnh</th>
                            <th class="text-center">Tiêu đề</th>
                            <th class="text-center">Thời gian đăng bài/ Trạng thái</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                            @foreach ($list_job as $key => $job)
                                <tr>
                                    <td class="text-center">
                                        @if ($job->avatar)
                                            <img src="{{ $job->avatar }}" alt=""
                                                style="width: 116px;height: auto;border-radius: 10px;">
                                        @endif
                                    </td>
                                    <td class="text-center"><a target="_blank" href="{{ 'https://mazii.net/job/detail/' . $job->id }}">{{ $job->title }}</a></td>
                                    <td class="text-center">{{ $job->created_at }}
                                        @if ($job->active)
                                            <span class="label label-success label_active" style="margin-left:30px;">Hiển
                                                thị</span>
                                        @else
                                            <span class="label label-danger label_active" style="margin-left:30px;">Đã
                                                ẩn</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('backend.jobs.edit',['job' => $job->id]) }}"><button class="btn btn-block btn-sm btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Xem/sửa bài viết</button></a>
                                        <a href="{{ route('backend.jobs.candidate',['job' => $job->id]) }}"><button class="btn btn-block btn-sm btn-primary"><i class="fa fa-user" aria-hidden="true"></i>Danh sách ứng viên</button></a>
                                        @if ($job->active)
                                            <button class="btn btn-block btn-sm btn-warning btn_active"
                                                data-active="{{ $job->active }}" data-id="{{ $job->id }}"><i class="fa fa-eye-slash" aria-hidden="true"></i>Ẩn bài
                                                viết</button>
                                        @else
                                            <button class="btn btn-block btn-sm btn-success btn_active"
                                                data-active="{{ $job->active }}" data-id="{{ $job->id }}"><i class="fa fa-eye" aria-hidden="true"></i>Hiển thị</button>
                                        @endif
                                        <button class="btn btn-block btn-sm btn-danger btn_del_job" data-id="{{ $job->id }}"><i class="fa fa-trash-o" aria-hidden="true"></i>Xoá</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $list_job->appends([
        'type' => Request::get('type'),
        'active' => Request::get('active'),
        'majors' => Request::get('majors'),
        'country' => Request::get('country'),
        'province' => Request::get('province'),
        'keyword' => Request::get('keyword'),
    ])->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>

@endsection
