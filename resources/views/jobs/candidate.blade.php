@extends('layouts.master')
@section('after-script')
    <script src="{{ url('myJs/jobs/candidate.js') }}"></script>
@endsection
@section('main') 
    <div class="col-md-12 col-xs-12" style="position: -webkit-sticky; position: sticky; top: 0;">
        <div class="x_panel">
            <div class="x_title">
                @include('includes.previous', ['back_link' => route('backend.jobs.index')])
                <h2   class="col-md-12 col-xs-12">Danh sách user ứng tuyển bài viết: <a target="_blank" href="https://mazii.net/job/detail/{{ $job->id }}" style="text-decoration: underline">{{$job->title}}</a> </h2>
                <br><small style="padding-left:20px;">Thời gian đăng bài: {{ $job->created_at }}</small><hr>
                <div class="row">
                    <div class="btn-group col-xs-2">
                        <b>Sắp xếp theo:</b>
                        <select name="type" class="form-control sort_select">
                            <option value="desc" {{ ($sort === 'desc' ? 'selected' : '') }}>Mới nhất</option>
                            <option value="asc" {{ ($sort === 'asc' ? 'selected' : '') }}>Cũ nhất</option>
                        </select>
                    </div>
                    <div class="col-md-2 pull-right">
                        <br>
                        <button class="btn btn-sm btn-primary btn_searh_keyword">Tìm kiếm</button>
                    </div>
                    <div class="col-md-3 pull-right">
                        <b>Tìm kiếm theo email:</b>
                        <input type="text" class="form-control input-search" name="search"
                            placeholder="Nhập email của ứng viên ..." value="{{ $keyword }}">
                    </div>
                </div>
                <div class="clearfix"></div><br>
            </div>
            <h5 class="col-md-8 col-xs-8"><b>Hiện tổng có: <b style="color: #DD1D1D;">{{ $total_userApplies }}</b> người đã gửi mail ứng tuyển cho bài viết này</b></h5><br>
            <div class="x_content">
                <div class="table-responsive">
                    <div class="text-center">{{ $list_users->links() }}</div>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <th class="text-center">STT</th>
                            <th class="text-center">Username</th>
                            <th class="text-center">Email đăng ký/ứng tuyển</th>
                            <th class="text-center">Thời gian gửi</th>
                            <th class="text-center">Thông tin</th>
                        </thead>
                        <tbody> 
                            @php
                                $stt = $list_users->firstItem() - 1;
                            @endphp
                            @foreach ($list_users as $key => $value)
                                <tr>
                                    <td  class="text-center">{{ $stt += 1 }}</td>
                                    <td  class="text-center">{{ $value->username }}</td>
                                    <td  class="text-center">{{ $value->pivot->user_email }}</td>
                                    <td  class="text-center">{{ $value->pivot->created_at }}</td>
                                    <td  class="text-center">
                                        <button class="btn btn-sm btn-primary btn_show_user" data-toggle="modal" data-target=".modal_profile" data-id="{{ $value->userId }}">Xem Profile</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $list_users->appends([
                            'sort' => Request::get('sort'),
                            'keyword' => Request::get('keyword')
                        ])->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>

    {{-- Modal Profile --}}
    <div class="modal fade bs-create-project-modal-sm modal_profile" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:500px;">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="">Thông tin Profile</h4>
                </div>
                <div class="modal-body">
                    <div >
                        <label >Họ tên: </label><span id="name" class="label_text"></span><br>
                        <label >Giới tính: </label> <span id="sex" class="label_text">Nam </span> <span class="pull-right"><label >Ngày sinh:</label> <span id="birthday" class="label_text"></span></span><br>
                        <label >Quốc gia:</label><span id="country" class="label_text"> </span> <span class="pull-right"><label >Level:</label> <span id="level" class="label_text"></span></span><br>
                        <label >Email: </label> <span id="email" class="label_text"> </span><br>
                        <label >Địa chỉ: </label> <span id="address" class="label_text"> </span><br>
                        <label >Số điện thoại:</label><span id="phone" class="label_text"> </span><br>
                        <label >Giới thiệu bản thân:</label><span id="introduction" class="label_text"> </span><br>
                    </div><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                </div>

            </div>
        </div>
    </div>

@endsection
