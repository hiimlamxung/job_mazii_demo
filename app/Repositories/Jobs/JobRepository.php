<?php

namespace App\Repositories\Jobs;

use App\Core\Repositories\BaseRepository;
use App\Models\Jobs\Job;
use App\Repositories\Jobs\Contract\JobRepositoryInterface;
use App\Models\Users\Profile;
use Illuminate\Http\UploadedFile;
use Image;


class JobRepository extends BaseRepository implements JobRepositoryInterface
{
    protected $model;

    public function __construct(Job $job)
    {
        parent::__construct($job);
        $this->model = $job;
    }

    public function classifyLocationParams(array $countries, array $provinces)
    {
        $countries = array_unique($countries);
        $provinces = array_unique($provinces);
        $data = [];
        $list_province = config('job.province');
        foreach ($countries as $country) {
            if (isset($list_province[$country])) {
                $data[$country] = [];

                foreach ($provinces as $province) {
                    if (isset($list_province[$country][$province])) {
                        $data[$country][] = $province;
                    }
                }
            }
        }
        return $data;
    }

    public function activeOrHideJob($id, $active)
    {
        $user = auth()->user();
        return $user->job()->where('id', $id)->update(['active' => $active]);
    }

    public function delJob($id)
    {
        $user = auth()->user();
        return $user->job()->where('id', $id)->delete();
    }

    public function storeJob(array $params)
    {
        if (isset($params['avatar'])) {
            $avatar = $params['avatar']; 
            unset($params['avatar']);
        }
        $job = $this->model->create($params);
        if ($job) {
            if (isset($avatar)) {
                $avatarName = time() . $job->id . '.jpg';
                $url_image = $this->uploadFTPCompany($avatar, $avatarName);
                $job->avatar = $url_image;
                $job->save();
            }
            return $job;
        }
        return false;
    }

    public function uploadFTPCompany(UploadedFile $file, $fileName)
    {
        $data = Image::make($file->path())->encode('jpg');
        $space = config("access.ftp_company.space");
        $path = $space . $fileName;
        $host = config("access.ftp_company.host") . $path;

        /* create a stream context telling PHP to overwrite the file */
        $options = array('ftp' => array('overwrite' => true));
        $stream = stream_context_create($options);
        $success = file_put_contents($host, $data, 0, $stream);

        return config("access.ftp_company.link") . $fileName;
    }

    public function updateJob(array $params,$id){
        if (isset($params['avatar'])) {
            $avatar = $params['avatar']; 
            unset($params['avatar']);
        } 
        $job = auth()->user()->job()->where('id',$id)->first();
        if($job){
            if($update = $job->update($params)){
                if (isset($avatar)) {
                    $avatarName = time() . $job->id . '.jpg';
                    $url_image = $this->uploadFTPCompany($avatar, $avatarName);
                    $job->avatar = $url_image;
                    $job->save();
                }
                return true;
            }
        }
        return false;
    }
}

