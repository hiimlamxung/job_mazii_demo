<?php

namespace App\Http\Requests\Jobs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        $list_type   = config('job.type');
        $list_country = array_keys(config('job.country'));
        $list_province = array_merge(array_keys(config('job.province.vn')),array_keys(config('job.province.jp')));
        return [
            'avatar'                            => 'mimes:png,jpg,jpeg,gif,svg|between:1,2560',
            'title'                             => 'required|min:10',
            'on_position'                       => 'required',
            'majors'                            => 'required|array',
            'majors.*'                          => 'exists:job_majors,id',
            'country'                           => 'required|array',
            'country.*'                         => 'in:'.implode(',',$list_country),
            'province'                          => 'array',
            'province.*'                        => 'in:'.implode(',',$list_province),
            'type'                              => 'required|in:'. implode(',',$list_type),
            'salary'                            => 'required',
            'description'                       => 'required|min:20',
            'require'                           => 'required|min:20',
            'benifit'                           => 'required|min:10'
        ];
    }

    public function messages(){
        return [
            'country.required' => 'Vui lòng chọn quốc gia',
            // 'province.required' => 'Vui lòng chọn tỉnh/thành phố',
            'title.required'    => 'Vui lòng nhập tiêu đề',
            'description.required' => 'Vui lòng nhập mô tả công việc',
            'require.required' => 'Vui lòng bổ sung trường "Yêu cầu ứng viên"',
            'benifit.required' => 'Vui lòng nhập trường "Quyền lợi được hưởng"',
            'type.required' => 'Vui lòng chọn Hình thức làm việc',
            'on_position.required' => 'Vui lòng nhập "Vị trí tuyển dụng',
            'majors.required'   =>  'Vui lòng chọn chuyên ngành',
            'salary.required'   => 'Vui lòng nhập mức lương',
            'avatar.mimes'  => 'Vui lòng chọn ảnh đại diện có định dạng: png,jpg,jpeg,gif,svg.',
            'avatar.between'  => 'Vui lòng chọn ảnh đại diện có kích cỡ <= 250MB',
            'title.min' => 'Tiêu đề bài viết tối thiểu ít nhất 10 ký tự',
            'description.min' => 'Mô tả công việc tối thiểu ít nhất 20 ký tự',
            'require.min' => 'Trường "Yêu cầu ứng viên" tối thiểu ít nhất 20 ký tự',
            'benifit.min' => 'Trường "Quyền lợi được hưởng" tối thiểu ít nhất 10 ký tự',
            'majors.array' => 'Chuyên ngành không đúng định dạng',
            'country.array' => 'Quốc gia không đúng định dạng',
            'province.array' => 'Tỉnh thành không đúng định dạng',
            'country.*.in' => 'giá trị quốc  gia không tồn tại',
            'province.*.in' => 'giá trị tỉnh thành không tồn tại',
            'majors.*.exists' => 'giá trị chuyên ngành không tồn tại',
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw new HttpResponseException(response()->json(['error' => true,'status' => 400, 'message' => $validator->errors()->first()],400));
    }
}

