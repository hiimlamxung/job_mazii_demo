<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Quản lý tuyển dụng</h3>
        <ul class="nav side-menu">
            <li class="{{ Request::is('jobs/*') ? 'active' : '' }}"><a><i class="fa fa-folder-open-o" aria-hidden="true"></i> Quản lý bài viết <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="{{ (url()->current() == route('backend.jobs.index') || Request::is('jobs/candidate/*')) ? 'active' : '' }}">
                        <a href="{{ route('backend.jobs.index') }}">Danh sách</a>
                    </li>
                    <li class="{{ (url()->current() == route('backend.jobs.create')) ? 'active' : '' }}">
                        <a href="{{ route('backend.jobs.create') }}">Đăng bài</a>
                    </li>
                </ul>
            </li>
            <li class="{{ (url()->current() == route('backend.accounts.myaccount')) ? 'active' : '' }}"><a href="{{ route('backend.accounts.myaccount') }}"><i class="fa fa-user" aria-hidden="true"></i> Tài khoản</a></li>
        </ul>
    </div>
        
</div>