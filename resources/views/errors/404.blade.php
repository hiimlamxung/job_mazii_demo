@extends('auth.layout')
@section('main')
<div class="col-md-12">
    <div class="col-middle">
      <div class="text-center">
        <h1 class="error-number">404</h1>
        <h2>Đường Link không hợp lệ, vui lòng quay trở lại. </h2>
        </p>
        <div ><br>
          <h3><a href="{{ route('backend.dashboard') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i>Trở về</a></h3>
          {{-- <form>
            <div class="col-xs-12 form-group pull-right top_search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
              </div>
            </div>
          </form> --}}
        </div>
      </div>
    </div>
</div>
@endsection