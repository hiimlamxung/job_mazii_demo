<?php

namespace App\Repositories\Jobs\Contract;

use App\Core\Repositories\Contract\BaseRepositoryInterface;
use Illuminate\Http\UploadedFile;
use App\Models\Users\UserMazii;

interface JobRepositoryInterface extends BaseRepositoryInterface {
    public function classifyLocationParams(array $countries, array $provinces);

    public function activeOrHideJob($id, $active);

    public function delJob($id);

    public function storeJob(array $params);

    public function updateJob(array $params,$id);

    public function uploadFTPCompany(UploadedFile $file, $fileName);
}