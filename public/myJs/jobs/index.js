$(function(){ 
    //load Tags
    var loadTags = function(selector,datas,place_holder){
        $(selector).select2({
            data: datas,
            tokenSeparators: [','], 
            placeholder: place_holder,
            /* the next 2 lines make sure the user can click away after typing and not lose the new tag */
            selectOnClose: false, 
            closeOnSelect: false
        });
    }
    loadTags('.tag_majors',dataMajors,'Nhập chuyên ngành');
    loadTags('.tag_province_vn',listVnProvince,'Nhập tỉnh thành');
    loadTags('.tag_province_jp',listJpProvince,'Nhập tỉnh thành');

    //Checkbox select Country
    $(".country").each(function(){
        let country = $(this).val();
        if(!$(this).is(':checked')){
            $(".tag_province_" + country).attr('disabled',true);
        }
    })
    $(".country").each(function(){
        let country = $(this).val();
        $(this).click(function(){
            if($(this).is(':checked')){
                $(".tag_province_" + country).removeAttr('disabled');
            }else{
                $(".tag_province_" + country).attr('disabled',true);
                $(".tag_province_" + country).val(null).trigger("change");
            }
        });
    })
   
    //Click filter
    $('.btn_filter').click(function(){
        var type = $('.type_select').val();
        var active = $('.active_select').val();
        var majors = $('.tag_majors').val();

        var country = [];
        $('input[name="country"]:checked').each(function(){
            country.push($(this).val());
        });
        var jp_province = $('.tag_province_jp').val();
        var vn_province = $('.tag_province_vn').val();

        //handle url
        var query_string = `?type=${type}&active=${active}`;
        if(majors && majors.length > 0){
            $.each(majors,function(index,value){
                query_string += `&majors[]=${value}`;
            })
        }
        if(country && country.length > 0){
            $.each(country,function(index,value){
                query_string += `&country[]=${value}`;
            })
        }
        if(jp_province && jp_province.length > 0){
            $.each(jp_province,function(index,value){
                query_string += `&province[]=${value}`;
            })
        }
        if(vn_province && vn_province.length > 0){
            $.each(vn_province,function(index,value){
                query_string += `&province[]=${value}`;
            })
        }
        window.location.href = url_list_job + query_string;
        
    })

    //click search keyword
    $('.btn_searh_keyword').click(function(){
        var keyw = $('.input-search').val();
        window.location.href = url_list_job + `?keyword=${keyw}`;
    });

    //click Show/Hide job
    $('body').on('click','.btn_active',function(){
        var id = $(this).attr('data-id');
        var active = $(this).attr('data-active');
        var data_active = (active == 1) ? 0 : 1;
        var _this = $(this);
        var label_active = _this.parent().prev().find('.label_active');
        $.ajax({
            type: "POST",
            url: url_active_job,
            data: {
                id: id,
                active: data_active
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response.status == 200){
                    notify('Thành công', 'success');
                    if(data_active == 1){
                        _this.removeClass('btn-success');
                        _this.addClass('btn-warning');
                        _this.html('<i class="fa fa-eye-slash" aria-hidden="true"></i>Ẩn bài viết');
                        _this.attr('data-active',data_active);
                        label_active.removeClass('label-danger');
                        label_active.addClass('label-success');
                        label_active.text('Hiển thị');
                    }else{
                        _this.removeClass('btn-warning');
                        _this.addClass('btn-success');
                        _this.html('<i class="fa fa-eye" aria-hidden="true"></i>Hiển thị');
                        _this.attr('data-active',data_active);
                        label_active.removeClass('label-success');
                        label_active.addClass('label-danger');
                        label_active.text('Đã ẩn');
                    }
                }
            }
        });
    });

    //click del job
    $(".btn_del_job").click(function(){
        var _this = $(this);
        var id = _this.attr('data-id');
        if(confirm('Bạn có muốn xóa bài viết tuyển dụng này không ?')){
            $.ajax({
                type: "DELETE",
                url: url_del_job,
                data: {
                    id:id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    if(response.status == 200){
                        _this.parent().parent().remove();
                        notify('Thành công', 'success');
                    }
                }
            });
        }
    });
})