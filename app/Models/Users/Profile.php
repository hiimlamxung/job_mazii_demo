<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $primaryKey = 'profile_id';
    protected $guarded = [];
    
    public function scopeActive($query){
        return $query->where('status', '<>', -1);
    }
}
