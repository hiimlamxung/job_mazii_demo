<?php

namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class LoginController extends Controller
{
    const _verifyed = 1;
    const _active = 1;

    public function index(Request $request){
        return view('auth.login');
    }

    public function login(Request $request){
        $user = User::with('profile:image')->where([
            'email' => $request->email,
            'password' => md5($request->password),
            'active' => self::_active,
            'user_company' => self::_verifyed
        ])->first();

        if($user){
            Auth::login($user);
            return redirect()->route('backend.dashboard');
        }else{
            return back()->withErrors(['msg' => 'Account does not have access!'])->withInput();
        }
    }
}
