<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Users\Profile;
use App\Models\Jobs\Job;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $guarded = [];
    protected $primaryKey = 'userId';

    /** Relationships */

    public function profile(){
        $list_column = ['profile_id', 'user_id', 'name', 'level','follow','image', 'info', 'address','phone','info','email',
        'job','birthday', 'need', 'country', 'sex','facebook','website','introduction','card_id','card_date','card_address'];
        return $this->hasOne(Profile::class, 'user_id')->select($list_column)->withDefault();
    }

    public function job(){
        return $this->hasMany(Job::class,'user_id','userId');
    }
    /** End relationships **/

    public function setPassWordAttribute($value){
        $this->attributes['password'] = md5($value);
    }
}
