<?php

namespace App\Http\Controllers\Backend\Accounts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Traits\ApiResponser;
use Javascript;
use App\Http\Requests\Accounts\UpdatePassWordRequest;

class AccountController extends Controller
{
    use ApiResponser;

    public function myaccount(){
        $account = auth()->user()->load('profile');

        JavaScript::put([
            'url_update_username' => route('backend.accounts.updateusername'),
            'url_update_password' => route('backend.accounts.updatepassword')
        ]);
        return view('accounts.myaccount',compact('account'));
    }

    public function updateUsername(Request $request){
        if($request->ajax()){
            $username = $request->username;
            $user = auth()->user();
            $user->username = $username;
            if($user->save()){
                return $this->success('Update success',200);
            }
            return $this->error('Update Failed',500);
        }
        return $this->error('Bad Request',400);
    }

    public function updatePassWord(UpdatePassWordRequest $request){
        if($request->ajax()){
            $user = auth()->user();
            $current_password = md5($request->get('current_password'));
            $new_password = $request->get('password');
            if($current_password != $user->password){
                return $this->error('Mật khẩu hiện tại không đúng',400);
            }
            $user->password = $new_password;  
            if($user->save()){
                return $this->success('success');
            }
        }
        return $this->error('Có lỗi xảy ra',400);
    }
}
