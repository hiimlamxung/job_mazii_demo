<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class HomeController extends Controller
{
    public function index(){
        return view('dashboard.index');
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('backend.index');
    }
}
