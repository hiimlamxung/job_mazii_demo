<?php

namespace App\Repositories\Accounts;

use App\Core\Repositories\BaseRepository;
use App\Repositories\Accounts\Contract\AccountRepositoryInterface;
use App\User;

class AccountRepository extends BaseRepository implements AccountRepositoryInterface
{
    protected $model;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->model = $user;
    }

}

