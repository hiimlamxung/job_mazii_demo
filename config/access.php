<?php

return [
    'account' => [
        'admin' => env('ADMIN_EMAIL'),
        'password' => env('ADMIN_PASSWORD')
    ],
    'password_default' => 'eupgroup.net',
    'ftp' => [
        'host' => env('FTP_HOST'),
        'space' => 'assets/mazii_data/user_data/',
        'link' => 'https://data.mazii.net/user_data/'
    ],
    'ftp_company' => [
        'host' => env('FTP_HOST'),
        'space' => 'assets/mazii_data/company_data/',
        'link' => 'https://data.mazii.net/company_data/'
    ],
];