<?php

namespace App\Repositories\Profile;

use App\Core\Repositories\BaseRepository;
use App\Repositories\Profile\Contract\ProfileRepositoryInterface;
use App\Models\Users\Profile;


class ProfileRepository extends BaseRepository implements ProfileRepositoryInterface {
    protected $model;

    public function __construct(Profile $profile)
    {
        parent::__construct($profile);
        $this->model = $profile;
    }

    public function getPrivateProfile($userId){
        $result = $this->model->whereUserId($userId)->active()->first()->private;
		$private = explode('-', $result);
		
		return (count($private) && $private) ? $private : [];
    }
    public function showProfileByUserId($userId){
        $profile = $this->model->whereUserId($userId)->active()->first();
        $privates = $this->getPrivateProfile($userId);
        foreach($privates as $private){
            $profile->{$private} = 'Không công khai';
        }
        return $profile;
    }
}