<footer>
    <div class="pull-right">
        Copyright © 2020-2021 <a href="#" target="_blank">TP</a> by <a href="https://mazii.net/" target="_blank">{{ config('app.name') }} Team</a>
    </div>
    <div class="clearfix"></div>
</footer>